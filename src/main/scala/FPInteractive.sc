import scala.annotation.tailrec

def fibonacciExpansive(x:Int): Int = {
  if(x <= 1) x
  else fibonacciExpansive(x - 1) + fibonacciExpansive(x - 2)
}

fibonacciExpansive(10)
// ----------
def fibonacciGenerative(n: Int):Int = {
  @tailrec
  def fib(n:Int, acc:List[Int], oldest:Int): Int = {
    if(acc.size > n ) acc.last
    else fib(n, acc :+ (acc.last + oldest), acc.last)
  }
  fib(n, List(0, 1), 0)
}
// ----------

fibonacciGenerative(10)

def fibonacciIterative(n: Int): Int = {
  @tailrec
  def fib(x: Int, prev:Int, acc:Int): Int = {
    if(x <= 1) acc
    else fib(x - 1, acc, prev + acc)
  }

  fib (n, 0, 1)
}

fibonacciIterative(10)
// ----------

// scala has type aliases just like Haskell
type OrderId = String
def processOrder(x: OrderId): Unit = {
  // do something with Order
}
// ----------

// it also has type-variables for creating polymorphic functions
// binarySearch asserts that the elements are sorted already...
def binarySearch[A:Numeric] (coll: Array[A], key: A, lessThan: (A, A) => Boolean): Option[A] = {

  def isEven(n:Int) = n % 2 == 0
  def pivotPosition(start:Int, end:Int):Int = {
    if(isEven(end - start)) (end - start) / 2 - 1 + start
    else (end - start) / 2 + start
  }

  @tailrec
  def loop(xs: Array[A], key: A, startIdx: Int, totalItems: Int): Option[A] = {
    val pivotIdx = pivotPosition(startIdx, totalItems)

    if (startIdx >= totalItems || pivotIdx < 0) None
    else {
      val pivotVal = xs(pivotIdx)

      if (pivotVal == key) Some(key)
      else if (lessThan(key, pivotVal))
        loop(xs, key, startIdx, pivotIdx - 1)
      else
        loop(xs, key, pivotIdx + 1, totalItems)
    }
  }

  loop(coll, key, 0, coll size)
}

// [1, 2, 3, 4] => [1] 2 [3 4]
// [1, 2, 3, 4, 5] => [1, 2]  3   [5, 9]

// some testing::
binarySearch(Array(1, 2, 3, 5, 9), 100, (x:Int, y:Int) => x < y)
// None

binarySearch(Array(1, 2, 3, 5, 9), 5, (x:Int, y:Int) => x < y)
// Some(5)

binarySearch(Array(1, 2, 3, 5, 9), -3, (x:Int, y:Int) => x < y)
// None

binarySearch(Array(1, 2, 3, 5, 9), 7, (x:Int, y:Int) => x < y)
// None

binarySearch(Array(1, 2, 3, 5, 9), 9, (x:Int, y:Int) => x < y)
// Some(9)

binarySearch(Array(1, 2, 3, 5), 4, (x:Int, y:Int) => x < y)
// None

// ---------------------
// Exercise:: implement compose fn
// def compose[A, B, C] (f: B => C, g: A => B): A => C
// (f . g)x = f(g(x))
def compose[A, B, C] (f: B => C, g: A => B): A => C = {
  (arg:A) => {
    val result = g(arg)
    f(result)
  }
}

// testing::
def toNumber(x:String): Int = {
  Integer.valueOf(x)
}
def addFive(x:Int): Int = {
  x + 5
}
compose(addFive, toNumber) ("20") // 25

// -------------------
def partial1[A, B, C] (x:A, f: (A, B) => C): B => C = {
  (y:B) => f (x, y)
}

def add(x:Int, y:Int): Int = x + y
partial1(4, add) (5) // 9


def curry[A, B, C] (f: (A, B) => C): A => (B => C) = {
  (a:A) => (b:B) => f(a, b)
}

curry(add) (2) (3) // 5


// remember that "=>" is right-associative so the parentheses are optional in this case
// def uncurry[A, B, C] (f: A => (B => C)): (A, B) => C
// =============
// Chapter 3:: Functional Data Structures:
// recursive type definition comparison::
// haskell::
// data List a = Nil | Cons a (List a)

sealed trait MyFunction
case object DefaultMyFunction extends MyFunction

object MyFunction {
  def flip[A, B, C] (f: (A, B) => C): (B, A) => C = (b: B, a: A) => f(a, b)
}

// f flip

// scala::
sealed trait MyList[+A]
case object Nil extends MyList[Nothing]
case class Cons[A] (head:A, tail: MyList[A]) extends MyList[A]

// the Nil and Cons declaration are actually Data-Constructors in haskell, whereas the Type itself: MyList
// can have 2 data-constructors in this case, based on the list-characteristics that a list can be either
// empty-list(symbolized as Nil) or it can be a valid container.
// now because we've tagged the MyList as "sealed" we continue in this single file with a "companion" object:
// The companion object basically provides a place where one can put "static-LIKE"(but not static) methods.
// Furthermore, a companion object, or companion module, has full access to the class members, including private ones.
// Companion objects are great for encapsulating things like factory methods.
// Instead of having to have, for example, Foo and FooFactory everywhere, you can have a class with a companion
// object take on the factory responsibilities.

object MyList {
  def sum(ints: MyList[Int]): Int = ints match {
    case Nil => 0
    case Cons(x,tail) => x + sum(tail)
  }

  def product(nums: MyList[Double]): Double = nums match {
    case Nil => 1  // identity operand for multiplication operation
    case Cons(0.0, _) => 0.0
    case Cons(x, tail) => x * product(tail)
  }

  // this will fold => from right(expand first, and then unfold from right), building the recipe for back-propagation
  // 1 + f(2 +
  // Cons(y,..) becomes f(y)
  def foldRight[A, B] (f: (A, B) => B, identity: B, xs: MyList[A]): B = xs match {
    case Nil => identity
    case Cons(y, ys) => f(y, foldRight(f, identity, ys))
  }

  // foldLeft => will do an iterative recursion(fold from left); as seen this is tail-recursive
  @tailrec
  def foldLeft[A, B] (f: (B, A) => B, identity: B, xs: MyList[A]): B = xs match {
    case Nil => identity
    case Cons(y, ys) => foldLeft(f, f(identity, y), ys)
  }

  def generateWhile[A] (seed: A, f: A => A , predicate: A => Boolean): MyList[A] = {
    if(predicate(seed))
      Cons(seed, generateWhile(f(seed), f, predicate))
    else Nil
  }

  def ssum (nums: MyList[Double]): Double = {
    foldRight((x:Double, y:Double) => (x + y), 0.0, nums)
  }

  def length[A] (xs: MyList[A]): Int = {
    foldRight((_:A, identity: Int) => 1 + identity, 0, xs)
  }

  def reverse[A] (xs: MyList[A]): MyList[A] = {
    foldLeft((ys: MyList[A], x: A) => Cons(x, ys), Nil, xs)
  }

  def tail[A] (xs:MyList[A]):MyList[A] = xs match {
    case Nil => Nil
    case Cons(_, ys) => ys
  }

  def drop[A] (xs:MyList[A], d:Int): MyList[A] = d match {
    case 0 => xs
    case _ => xs match {
      case Nil => Nil
      case Cons(_, ys) => drop(ys, d - 1)
    }
  }

  def dropWhile[A] (xs: MyList[A]) (f: A => Boolean): MyList[A] = xs match {
    case Nil => Nil
    case Cons(y, ys) => {
      if (f(y)) dropWhile(ys) (f)
      else xs
    }
  }

  def setHead[A] (xs:MyList[A], x:A): MyList[A] = xs match {
    case Nil => Nil
    case Cons(y, ys) => Cons(x, ys)
  }

  def append[A] (xs:MyList[A], ys:MyList[A]): MyList[A] = xs match {
    case Nil => ys // just return the second
    case Cons(x, rest) => Cons(x, append(rest, ys))
  }

  def init[A] (xs: MyList[A]): MyList[A] = xs match {
    case Nil => Nil
    case Cons(_, Nil) => Nil
    case Cons(y, ys) => Cons(y, init(ys))
  }

  def head[A] (xs: MyList[A]): Option[A] = xs match {
    case Nil => None
    case Cons(y, _) => Some(y)
  }

  // build a recipe:: => foldRight is tail recursive since, foldLeft is tail-recursive
  // instead of passing the "identity-value", we're passing an identity-fn of B => B to foldLeft
  // inside the "merge-fn" we're building the recipe, that "expands" the fn-applications of those id-fns
  // each application will return another wrapped fn of that initial identityFn: the result of applying the merge-fn
  // the result of the foldLeft is a function x => f(a, f(b, f(c, x))), which, when applied to z, gives f(a, f(b, f(c, z)))
  def foldRightViaFoldLeft[A, B] (f: (A, B) => B, seed: B, xs:MyList[A]): B = {
    // note that [seed] for foldLeft is that single-arg fn that takes the initial
    // seed and uses it to backpropagate the collapsing calls from RIGHT
    // the "merge-fn" will return another single-arg(updated-seed) lambda that wraps the actual merge-computation
    def recipeFn = MyList
      .foldLeft((g: B => B, each: A) => (backSeed: B) => g(f(each, backSeed)), (xs: B) => xs, xs)
    recipeFn(seed)  // this will bootstrap the computation passing: seed to the right-most fn
  }

  // so map is written through foldRight, which is written in terms of foldLeft ===>
  // therefore [map] is a tail call operation
  def map[A, B] (f: A => B, xs: MyList[A]): MyList[B] = {
    MyList.foldRightViaFoldLeft((x: A, identity: MyList[B]) => Cons(f(x), identity), MyList(), xs)
  }

  // EXERCISE 15 (hard): Write a function that concatenates a list of lists into a single list.
  // Its runtime should be linear in the total length of all lists.
  // MyList(MyList(1, 2), MyList(3, 4)) => MyList(1, 2, 3, 4)
  def flatten[A] (xs: MyList[MyList[A]]): MyList[A] = {
    foldLeft(
      (identityXs: MyList[A], eachXs: MyList[A]) => MyList.append(identityXs, eachXs), MyList(), xs)
  }

  // filter can also be expressed through foldLeft => foldLeft is the thing...
  def filter[A] (test: A => Boolean, xs: MyList[A]): MyList[A] = {
    MyList.foldLeft(
      (ys: MyList[A], x: A) => {
        if(test(x)) Cons(x, ys)
        else ys
      }, Nil, xs)
  }

  // flatMap is the notorious monadic operation:
  // instead (like map) takes a [A] => List[B], not a single B
  // class T[+A] {
  //  def flatMap[B](f: A ⇒ T[B]): T[B]
  //  def map[B](f: (A) ⇒ B): T[B]
  // }
  def flatMap[A, B] (f: A => MyList[B], xs: MyList[A]): MyList[B] = {
    MyList.foldRightViaFoldLeft((x: A, acc: MyList[B]) => MyList.append(f(x), acc), Nil, xs)
  }

  // i could have expressed flatMap through foldLeft directly but then the arguments for MyList.append
  // should have been flipped:
  def flatMapViaFoldLeft[A, B] (f: A => MyList[B], xs: MyList[A]): MyList[B] = {
    MyList.foldLeft((acc: MyList[B], x: A) => MyList.append(acc, f(x)), Nil, xs)
  }

  // non-tailrec version
  def zip[A, B, C] (f: (A, B) => C, xs: MyList[A], ys: MyList[B]): MyList[C] = {
    val t = (xs, ys)
    t match {
      case (Nil, _) => Nil
      case (Cons(x, tailx), Cons(y, taily)) => Cons(f(x, y), zip(f, tailx, taily))
    }
  }

  // applying the same clever logic of building the lambda-recipe calls
  // ==> simulate the behaviour of an expansive recursion && having tailrec optimization
  def zipTailRecClever[A, B, C] (xs: MyList[A], ys: MyList[B]) (f: (A, B) => C) = {

    @tailrec
    def _zip[A, B, C](tp: (MyList[A], MyList[B]), seedFn: MyList[C] => MyList[C])(f: (A, B) => C)
    : MyList[C] => MyList[C]
    = tp match {
      case (Nil, _) => seedFn
      case (Cons(x, tailx), Cons(y, taily))
      => _zip((tailx, taily), (l:MyList[C]) => seedFn(Cons(f(x, y), l))) (f)
    }

    val t = (xs, ys) // to be able to do destructuring via pattern matching...
    val recipeFn = _zip(t, (xs: MyList[C]) => xs) (f)

    recipeFn(Nil) // bootstrap the whole recipe of lambda calls
  }

  // example of a variadic function:: this companion object defines a factory method: apply
  // Note: the _* informs the compiler to transform the Seq => Variadic parameter required by the
  // apply fn constructor
  def apply[A] (varargs: A*): MyList[A] = {
    if(varargs.isEmpty) Nil
    else Cons(varargs head, apply(varargs tail: _*))
  }
}

val myList = Cons(1, Cons (2, (Cons (3, Nil))))
MyList.sum(myList)

val ml = MyList(1,2,3,4,5) // MyList[Int] = Cons(1,Cons(2,Cons(3,Cons(4,Cons(5,Nil)))))
MyList.drop(ml, 2)         //  MyList[Int] = Cons(3,Cons(4,Cons(5,Nil)))

MyList.dropWhile(ml)(x => x < 3) // MyList[Int] = Cons(3,Cons(4,Cons(5,Nil)))

// B: List[String], A: Int
MyList.foldLeft((identity:List[String], x:Int) => "is:" + x.toString :: identity, List(), MyList(1,2,3))
// => List[String] = List(is:3, is:2, is:1)

MyList.ssum(MyList(1,2,3)) // Double = 6.0

MyList.generateWhile(1, (x:Int) => x + 1, (x:Int) => x < 10)
// MyList[Int] = Cons(1,Cons(2,Cons(3,Cons(4,Cons(5,Cons(6,Cons(7,Cons(8,Cons(9,Nil)))))))))

// A trait is an abstract interface that may optionally contain implementations of some methods.
// current use case does not declare any methods on MyList trait.

// +A => means the type-variable is relying on parametric polymorphism, in that + indicates the
// covariance of "A".
// in scala: covariance is much more powerful than in java: list[dog] is a list[animal]
// that is if dog is an animal.
// because Nil is declared to be: List[Nothing], Nothing is a special type in scala, that inherits
// from any trait, therefore Nil can be an empty list of: List[Double], List[String]...and so on.

// recursive data types like List (which refers to itself recursively in its Cons data constructor).

// Companion objects in Scala
// We will often declare a companion object in addition to our data type and its data constructors.
// This is just an object with the same name as the data type (in this case List) where we put
// various convenience functions for creating or working with values of the data type.

// Pattern matching:: descend into the structure of the expression it examines and extract subexpressions of that structure

// -----
// ##### All about "apply" semantics::
// Every function in Scala can be represented as an object.
// Every function also has an OO type: for instance, a function from Int => Int will have OO type of Function1[Int,Int].

val f = (x:Int) => x + 1
// assigning the function literal to a constant, now "f" references the Object Function[Int, Int]
// since everything is an object in scala.
// now comes the interesting part, because "f" is a reference to an object, but also can be considered
// as a reference to the function-literal, "f" can call .toString() inherited from [Any] super type.
// this would be impossible with pure-functions ==> "Apply" closes the gap between functional paradigms
// and OO.
f.toString()
// or we can define a new Function[Int, Int] Object, by calling "compose" METHOD on "f".
val g = f.compose((x: Int) => x - 1)
// now if we want to "apply the function to its arguments" we would:
g.apply(2)

/*********
Writing f.apply(args) every time you want to execute a function represented as an object is the Object-Oriented way
, but would add a lot of clutter to the code without adding much additional information and it would be nice
to be able to use more standard notation, such as f(args).
That's where Scala compiler steps in and whenever we have a reference f to a function-object
and write f (args) to apply arguments to the represented function the compiler silently expands
f (args) to the object method call f.apply (args).

 !!!! Every function in Scala can be treated as an object and it works the other way too
 - every object can be treated as a function, provided it has the [apply] method.
 *********/
object Foo {
  var x = 5

  def apply(y:Int): Int = x + y
}
Foo (9) // 14 -> function notation
// So apply method is just a handy way of closing the gap between functions and objects in Scala.

// ========================
// Implement the function tail for "removing" the first element of a List. Notice the function takes constant time. What are different choices you could make in your implementation if the List is Nil
def tail[A] (xs:MyList[A]):MyList[A] = xs match {
  case Nil => Nil
  case Cons(_, ys) => ys
}
// as we clearly see this indeed takes constant time, since it's just a destructuring via pattern
// matching and returning the new list

// generalize tail to drop [n] elements from the collection::
def drop[A] (xs:MyList[A], d:Int): MyList[A] = d match {
  case 0 => xs
  case _ => xs match {
    case Nil => Nil
    case Cons(_, ys) => drop(ys, d - 1)
  }
}

// Implement dropWhile,10 which removes elements from the List prefix as long as they match a predicate.
// Note that these functions take time proportional ONLY to the number of items being removed.
// ==> THEY DO NOT REQUIRE ANY COPY OF THE LIST => time consuming operation...
def dropWhile[A] (xs: MyList[A]) (f: A => Boolean): MyList[A] = xs match {
  case Nil => Nil
  case Cons(y, ys) => {
    if (f(y)) dropWhile(ys) (f)
    else xs
  }
}

MyList.dropWhile(ml)(x => x < 3) // MyList[Int] = Cons(3,Cons(4,Cons(5,Nil)))
// the grouping feature, allows for helping the compiler type inference in that as you see, [A] is
// abstract type variable which does NOT have the "<" operator, but because we're using the grouping-args
// feature, we're helping the compiler correctly infer that the Types for [A], respectively x is an Int,
// which does have the "<" math operators.
// this also opens the partial application, in that we can lock the container and supply LATER
// the predicate-fn that does the drop.
// as stated, else: MyList.dropWhile(ml, (x:Int) => x < 3)
// this is because Scala does NOT have full Type Inference like haskell.

// exercise 5:
def setHead[A] (xs:MyList[A], x:A): MyList[A] = xs match {
  case Nil => Nil
  case Cons(y, ys) => Cons(x, ys)
}

MyList.setHead(ml, 99) // Cons(99,Cons(2,Cons(3,Cons(4,Cons(5,Nil)))))

// implement the append fn which joins two lists together
// notice that the running time is directly influenced by the number of elements the [xs] has.
def append[A] (xs:MyList[A], ys:MyList[A]): MyList[A] = xs match {
  case Nil => ys // just return the second
  case Cons(x, rest) => Cons(x, append(rest, ys))
}

  MyList.append(MyList(8, 9), MyList(10, 11)) // Cons(8,Cons(9,Cons(10,Cons(11,Nil))))

  // exercise 6:
  // Not everything works out so nicely.
  // Implement a function, init, which returns a List consisting of all but the last element of a List.
  // So, given List(1,2,3,4), init will return List(1,2,3).
  // Why can't this function be implemented in constant time like tail?
  def init[A] (xs: MyList[A]): MyList[A] = xs match {
    case Nil => Nil
    case Cons(_, Nil) => Nil
    case Cons(y, ys) => Cons(y, init(ys))
  }

  MyList.init(MyList(1, 2, 3, 4))   // MyList[Int] = Cons(1,Cons(2,Cons(3,Nil)))
  // being a singly-linked-list the last element in order to be dropped the whole list needs to be
  // traversed, that is reconstructed.

  def head[A] (xs: MyList[A]): Option[A] = xs match {
    case Nil => None
    case Cons(y, _) => Some(y)
  }

  MyList.head(MyList(1, 2, 3)) // Some(1)
  MyList.head(Nil) // None

  // Because of the structure of a singly-linked list, any time we want to replace the tail of a Cons, even if it is the last Cons in the list, we must copy all the previous Cons objects. Writing purely functional data structures that support different operations efficiently is all about finding clever ways to exploit data sharing, which often means working with more tree-like data structures

  // EXERCISE 7: Can product implemented using foldRight immediately halt the recursion and return 0.0
  // if it encounters a 0.0? Why or why not?
  // foldRight is an "exhaustive" operation that is built around the semantic that a List can have
  // that is a list can be empty or non-empty(populated) container. Therefore there's NO other special logic
  // that deals with short-circuiting operation other than the list is empty. Based on the standard
  // foldRight type signature, there's no way we can short-circuit the entire right-fold process without
  // introducing some "value-based-logic" in this process. If foldRight is enhanced with a predicate
  // for this short-circuit step, it will be possible to stop the right-folding process, else
  // not. <== congrats great answer... :)

  // ===================
  def length[A] (xs: MyList[A]): Int = {
    MyList.foldRight((_:A, identity: Int) => 1 + identity, 0, xs)
  }
  MyList.length (MyList("foo", "bar")) // 2

  // =====================
  // EXERCISE 12: Write a function that returns the reverse of a list
  def reverse[A] (xs: MyList[A]): MyList[A] = {
    MyList.foldLeft((ys: MyList[A], x: A) => Cons(x, ys), Nil, xs)
  }

  reverse(MyList(1, 2, 3, 4))

  def flip[A, B, C] (f: (A, B) => C): (B, A) => C  = (b: B, a: A) => f(a, b)

  // EXERCISE 13 (hard): Can you write foldRight in terms of foldLeft?
  def foldRightViaFoldLeft1[A, B] (f: (A, B) => B, identity: B, xs: MyList[A]): B = {
    MyList.foldLeft(flip(f), identity, xs)
  }

  foldRightViaFoldLeft1((x: Int, y: Int) => x + y, 0, MyList(1, 2, 3)) // 6
  foldRightViaFoldLeft1((x: Int, xs: MyList[Int]) => Cons(x, xs), MyList(), MyList(1, 2, 3))
  // Cons(3,Cons(2,Cons(1,Nil)))
  // => not quite the same list being reversed...

  // build a recipe:: => foldRight is tail recursive since, foldLeft is tail-recursive
  def foldRightTailRec[A, B] (f: (A, B) => B, identity: B, xs: MyList[A]): B = {
    def recipeFn = MyList
      .foldLeft((identityFn: B => B, x: A) => (y: B) => identityFn(f(x, y)), (x: B) => x, xs)
    recipeFn(identity)
  }

  foldRightTailRec((x: Int, xs: MyList[Int]) => Cons(x, xs), MyList(), MyList(1, 2, 3))

  // EXERCISE 15 (hard): Write a function that concatenates a list of lists into a single list.
  // Its runtime should be linear in the total length of all lists.
  // MyList(MyList(1, 2), MyList(3, 4)) => MyList(1, 2, 3, 4)
  def flatten[A] (xs: MyList[MyList[A]]): MyList[A] = {
    MyList.foldLeft(
      (identityXs: MyList[A], eachXs: MyList[A]) => MyList.append(identityXs, eachXs), MyList(), xs)
  }

flatten(MyList(MyList(1, 2), MyList(3, 4))) //  MyList[Int] = Cons(1,Cons(2,Cons(3,Cons(4,Nil))))

// ================
// The main difference between the List developed here and the standard library version is that Cons is called ::, which is right-associative
// all operators ending in : are right-associative

// EXERCISE 18: Write a function map, that generalizes modifying each element in a list while maintaining the structure of the list
// a [map] fn is called homomorphic since it doesn't change the STRUCTURE of the domain.
// the List[A] => List[B], preserves the structure.
// unfortunatelly this reverses the list, since [Cons] adds to the head, and foldLeft means
// folding from left => right, last element will be the first in the final list.

def foldRightViaFoldLeft2[A, B] (f: (A, B) => B, identity: B, xs: MyList[A]): B = {
  MyList.foldLeft(flip(f), identity, xs)
}

def map1[A, B] (f: A => B, xs: MyList[A]): MyList[B] = {
 foldRightViaFoldLeft2((x: A, identity: MyList[B]) => Cons(f(x), identity), MyList(), xs)
}

map1((x: Int) => x + 1, MyList(1, 2, 3)) // MyList[Int] = Cons(4,Cons(3,Cons(2,Nil)))

// so map is written through foldRight, which is written in terms of foldLeft ===>
// therefore [map] is a tail call operation
def map[A, B] (f: A => B, xs: MyList[A]): MyList[B] = {
  MyList.foldRightViaFoldLeft((x: A, identity: MyList[B]) => Cons(f(x), identity), MyList(), xs)
}

map((x: Int) => x + 1, MyList(1, 2, 3)) // MyList[Int] = Cons(4,Cons(3,Cons(2,Nil)))

// foldLeft we now that is a tailrec fn, in that it folds FROM left, based on an iterative
// recursion, it passes the previous updated state to the next recursion
// foldRight expands first the recursion-calls until the edge-case is encountered,
// moment that will start fold FROM right(edge-case)
// foldRight is based on frames accumulation on stack and is in this case quite limited
// implementing other abstract fns like [map] based on stack-frames is not wise at all
// writing foldRight in terms of foldLeft is quite some mind-twisted challenge, but
// the idea is to INTRODUCE some sort of mechanism that instead to accumulate immediately
// via foldLeft, we need to SIMULATE the behavior that foldRight has(expand first, and then
// collapse from RIGHT). This mechanism is in fact a recipe ==> 2 steps to make it work:
// 1) use foldLeft to create the recipe, that is the call expansion till edge case
//    think of some "delayed" computations/wrapped fns, so our seed will be a single-arg
//    fn(that takes the initial seed)
// 2) trigger the recipe computation
// NOTE: that the whole magic is required because our MyList[A] ADT is FIFO
def foldRightViaFoldLeft[A, B] (f: (A, B) => B, seed: B, xs:MyList[A]): B = {
  // note that [seed] for foldLeft is that single-arg fn that takes the initial
  // seed and uses it to backpropagate the collapsing calls from RIGHT
  // the "merge-fn" will return another single-arg(updated-seed) lambda that wraps the actual merge-computation
  def recipeFn = MyList.foldLeft((g: B => B, each: A) => (backSeed: B) => g(f(each, backSeed)), (s: B) => s, xs)
  recipeFn(seed)  // this will bootstrap the computation passing: seed to the right-most fn
}

def lmap[A, B] (f: A => B, xs: MyList[A]): MyList[B] = {
  foldRightViaFoldLeft((x: A, ys:MyList[B]) => Cons (f(x), ys), Nil, xs)
}

lmap((x:Int) => x + 1, MyList(1, 2, 3)) // works nice!!

// ----------------
// filter can also be expressed through foldLeft => foldLeft is the thing...
def filter[A] (test: A => Boolean, xs: MyList[A]): MyList[A] = {
  MyList.foldLeft(
          (ys: MyList[A], x: A) => {
            if(test(x)) Cons(x, ys)
            else ys
          }, Nil, xs)
}

filter((x: String) => x == "claudiu", MyList("claudiu", "cosar"))
// MyList[String] = Cons(claudiu,Nil)

// -----------------
// flatMap is the notorious monadic operation:
// insead (like map) takes a [A] => List[B], not a single B
// class T[+A] {
//  def flatMap[B](f: A ⇒ T[B]): T[B]
//  def map[B](f: (A) ⇒ B): T[B]
// }
def flatMap[A, B] (f: A => MyList[B], xs: MyList[A]): MyList[B] = {
  MyList.foldRightViaFoldLeft((x: A, acc: MyList[B]) => MyList.append(f(x), acc), Nil, xs)
}

flatMap((x: Int) => MyList(x, x), MyList(1, 2, 3))
// MyList[Int] = Cons(1,Cons(1,Cons(2,Cons(2,Cons(3,Cons(3,Nil))))))

// i could have expressed flatMap through foldLeft directly but then the arguments for MyList.append
// should have been flipped:
def flatMapViaFoldLeft[A, B] (f: A => MyList[B], xs: MyList[A]): MyList[B] = {
  MyList.foldLeft((acc: MyList[B], x: A) => MyList.append(acc, f(x)), Nil, xs)
}

// and the same result, we're flipping the args to MyList.append
flatMapViaFoldLeft((x: Int) => MyList(x, x), MyList(1, 2, 3))
// MyList[Int] = Cons(1,Cons(1,Cons(2,Cons(2,Cons(3,Cons(3,Nil))))))

// write zip::
def zip[A, B, C] (f: (A, B) => C, xs: MyList[A], ys: MyList[B]): MyList[C] = {
  val t = (xs, ys)
  t match {
    case (Nil, _) => Nil
    case (Cons(x, tailx), Cons(y, taily)) => Cons(f(x, y), zip(f, tailx, taily))
  }
}

  zip((x: Int, y: Int) => x + y, MyList(1, 2, 3), MyList(4, 5, 6))

  // this version of [zip] is not tail-recursive, can you write it tail-recursively?
  // folding from left => a FIFO ds is not suitable => it reverses the ds
  def zipViaFoldLeft[A, B, C] (xs: MyList[A], ys: MyList[B]) (f: (A, B) => C) = {

    @tailrec
    def _zipTailRec[A, B, C](tp: (MyList[A], MyList[B]), seed: MyList[C])(f: (A, B) => C): MyList[C]
      = tp match {
        case (Nil, _) => seed
        case (Cons(x, tailx), Cons(y, taily))
            => _zipTailRec((tailx, taily), Cons(f(x, y), seed))(f)
      }

    val t = (xs, ys) // to be able to do destructuring via pattern matching...
    _zipTailRec(t, Nil) (f)
  }

  zipViaFoldLeft(MyList(1, 2, 3), MyList(4, 5, 6)) ((x, y) => x + y)
  // MyList[Int] = Cons(9,Cons(7,Cons(5,Nil))) => tail-recursive without building the
  // recipe of fn lambdas, will be appropriate for LIFO type of data-structures

  // but if we apply the same clever logic of building the lambda-recipe calls, then
  // we simulate the behaviour of an expansive recursion::
  def zipTailRecClever[A, B, C] (xs: MyList[A], ys: MyList[B]) (f: (A, B) => C) = {

    @tailrec
    def _zip[A, B, C](tp: (MyList[A], MyList[B]), seedFn: MyList[C] => MyList[C])(f: (A, B) => C)
      : MyList[C] => MyList[C]
      = tp match {
        case (Nil, _) => seedFn
        case (Cons(x, tailx), Cons(y, taily))
          => _zip((tailx, taily), (l:MyList[C]) => seedFn(Cons(f(x, y), l))) (f)
    }

    val t = (xs, ys) // to be able to do destructuring via pattern matching...
    val recipeFn = _zip(t, (xs: MyList[C]) => xs) (f)

    // bootstrap the whole recipe of lambda calls
    recipeFn(Nil)
  }

zipTailRecClever(MyList(1, 2, 3), MyList(4, 5, 6)) ((x, y) => x + y)
// MyList[Int] = Cons(5,Cons(7,Cons(9,Nil)))
// => BOOOYAAA :)) nice! no indexes just: pattern-matching/destructuring, recursion
// and a clever foldRight-emulation via lambda-creation=> all yielding a tail-recursive
// fn
